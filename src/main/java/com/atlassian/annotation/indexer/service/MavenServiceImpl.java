package com.atlassian.annotation.indexer.service;

import com.atlassian.annotation.indexer.model.atr.TestGav;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Base64;

@Service
public class MavenServiceImpl implements MavenService {

    @Value("${nexus.url}")
    private String nexusUrl;

    @Value("${nexus.username}")
    private String nexusUsername;

    @Value("${nexus.password}")
    private String nexusPassword;

    private static final Logger log = LoggerFactory.getLogger(MavenServiceImpl.class);

    public InputStream getMavenArtifact(TestGav testGav) throws FileNotFoundException {
        if (! testGav.isIgnoreTestArtifact()) {
            try {
                return getMavenArtifactByClassifier(testGav, "tests");
            } catch (FileNotFoundException e) {
                return getMavenArtifactByClassifier(testGav, "");
            }
        } else {
            return getMavenArtifactByClassifier(testGav, "");
        }
    }

    private String getBase64BasicHttpAuth() {
        return new String(Base64.getEncoder().encode(String.format("%s:%s", nexusUsername, nexusPassword).getBytes()));
    }

    private InputStream getMavenArtifactByClassifier(TestGav testGav, String classifier) throws FileNotFoundException {
        String downloadUrl = "";
        try {
            downloadUrl+= String.format("%s/service/local/artifact/maven/redirect", nexusUrl);
            downloadUrl += String.format("?g=%s&a=%s&v=%s&c=%s&r=internal", testGav.getGroupId(), testGav.getArtifactId(), testGav.getVersion(), classifier);
            URL url = new URL(downloadUrl);
            URLConnection connection = url.openConnection();
            if (!"".equals(nexusUsername)) {
                connection.setRequestProperty("Authorization", "Basic " + getBase64BasicHttpAuth());
            }
            return connection.getInputStream();
        } catch (IOException e) {
            if (e instanceof FileNotFoundException) {
                throw (FileNotFoundException)e;
            }
        }
        return null;

    }


}
