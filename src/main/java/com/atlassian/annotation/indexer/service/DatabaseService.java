package com.atlassian.annotation.indexer.service;

import com.atlassian.annotation.indexer.model.db.DBTest;
import com.atlassian.annotation.indexer.model.tests.Tests;
import com.atlassian.annotation.indexer.model.tests.TotalRow;

import java.util.List;

public interface DatabaseService  {

    void addTests(Tests tests, String testType);

    Tests getAllTests();

    List<TotalRow> getTestCountByType();

    List<TotalRow> getTestCountByCategory();

    List<TotalRow> getTestCountByCategory(String search);

    Tests getTestsByType(String testType);

    Tests getTestsByCategory(String category);

    Tests getTestsByType(String testType, String search);

    Tests getTestsByCategory(String category, String search);

    List<String> getTestCategories();

    List<String> getTestTypes();

}
