package com.atlassian.annotation.indexer.controller;

import com.atlassian.annotation.indexer.service.DatabaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ThymeleafController {

    @Autowired
    DatabaseService databaseService;

    @RequestMapping("/")
    public String root() {
        return "redirect:/listCategories";
    }

    @RequestMapping("/listCategories")
    public String listCategories(Model model, @RequestParam(required=false) String search) {
        if (search == null || StringUtils.isEmpty(search)) {
            model.addAttribute("rows", databaseService.getTestCountByCategory());
        } else {
            model.addAttribute("search", search);
            model.addAttribute("rows", databaseService.getTestCountByCategory(search));
        }
        model.addAttribute("testCount", true);
        model.addAttribute("categories", true);
        return "thyme/list";
    }

    @RequestMapping("/listSuites")
    public String listSuite(Model model) {
        model.addAttribute("rows", databaseService.getTestCountByType());
        model.addAttribute("testCount", true);
        model.addAttribute("types", true);
        return "thyme/list";
    }

    @RequestMapping("/listTests")
    public String listTests(Model model, @RequestParam(required=false) String cat, @RequestParam(required=false) String type, @RequestParam(required=false) String search) {
        if (! StringUtils.isEmpty(cat)) {
            model.addAttribute("cat", cat);
            if (! StringUtils.isEmpty(search)) {
                model.addAttribute("search", search);
                model.addAttribute("test", databaseService.getTestsByCategory(cat, search));
            } else {
                model.addAttribute("test", databaseService.getTestsByCategory(cat));
            }
        } else if (! StringUtils.isEmpty(type)) {
            model.addAttribute("type", type);
            if (! StringUtils.isEmpty(search)) {
                model.addAttribute("search", search);
                model.addAttribute("test", databaseService.getTestsByType(type, search));
            } else {
                model.addAttribute("test", databaseService.getTestsByType(type));
            }
        }
        model.addAttribute("testList", true);
        return "thyme/list";
    }

}
