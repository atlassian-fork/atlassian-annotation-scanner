package com.atlassian.annotation.indexer.rest;

import com.atlassian.annotation.indexer.model.atr.Gavs;
import com.atlassian.annotation.indexer.model.tests.Tests;
import com.atlassian.annotation.indexer.model.tests.TotalRow;
import com.atlassian.annotation.indexer.service.DatabaseService;
import com.atlassian.annotation.indexer.service.IndexService;

import com.timgroup.statsd.NonBlockingStatsDClient;
import com.timgroup.statsd.StatsDClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/index")
public class IndexResource {

    private static final Logger log = LoggerFactory.getLogger(IndexResource.class);

    @Autowired
    @Qualifier(value = "indexServiceImpl")
    IndexService indexService;

    @Autowired
    DatabaseService databaseService;

    private final StatsDClient statsDClient;

    @Autowired
    public IndexResource(@Value("${statsd.url}") String statsd) {
        this.statsDClient = new NonBlockingStatsDClient("atlassian-annotation-scanner", statsd, 8125);
    }

    @RequestMapping(path = "indexGavs/{testType}", method = RequestMethod.POST)
    public String downloadGavs(@RequestBody Gavs gavs, @PathVariable String testType) {
        indexService.indexTests(gavs, testType);
        statsDClient.incrementCounter("indexGavs");
        return "index triggered, check logs";
    }

    @RequestMapping(value = "artifact/{testType}", method = RequestMethod.POST)
    public String downloadArtifact(@PathVariable String testType, @RequestParam("file") MultipartFile file) throws IOException {
        InputStream targetStream = file.getInputStream();
        Pattern p = Pattern.compile("(\\S+)(\\-+)(\\d)");
        Matcher m = p.matcher(file.getOriginalFilename());
        m.find();
        indexService.indexTests(targetStream, testType, m.group(1));
        statsDClient.incrementCounter("indexArtifact");
        return "index triggered, check logs";
    }

    @RequestMapping(path = "testByType", method=RequestMethod.GET)
    public List<TotalRow> testByType() {
        statsDClient.incrementCounter("SuiteViewSeen");
        return databaseService.getTestCountByType();
    }

    @RequestMapping(path = "testByCategory",method=RequestMethod.GET)
    public List<TotalRow> testByCategory() {
        statsDClient.incrementCounter("CategortViewSeen");
        return databaseService.getTestCountByCategory();
    }

    @RequestMapping(path = "allTests", method = RequestMethod.GET)
    public Tests getAllTests() { return databaseService.getAllTests(); }

    @RequestMapping(path = "getTestsByType/{testType}", method=RequestMethod.GET)
    public Tests getTestsByType(@PathVariable String testType) {
        statsDClient.incrementCounter("requestTestsbyType");
        return databaseService.getTestsByType(testType);
    }

    @RequestMapping(path = "getTestsByCategory/{category:.+}", method=RequestMethod.GET)
    public Tests getTestsByCategory(@PathVariable String category) {
        statsDClient.incrementCounter("requestTestsbyCategory");
        return databaseService.getTestsByCategory(category);
    }

    @RequestMapping(path = "getTestCategories", method=RequestMethod.GET)
    public List<String> getTestsCategories() {
        statsDClient.incrementCounter("searchForCategory");
        return databaseService.getTestCategories();
    }

    @RequestMapping(path = "getTestTypes", method=RequestMethod.GET)
    public List<String> getTestTypes() {
        statsDClient.incrementCounter("searchForType");
        return databaseService.getTestTypes();
    }
}
