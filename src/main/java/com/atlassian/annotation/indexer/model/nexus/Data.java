package com.atlassian.annotation.indexer.model.nexus;

public class Data {

    private Artifact[] artifact;

    public Artifact[] getArtifact() {
        return artifact;
    }

    public void setArtifact(Artifact[] artifact) {
        this.artifact = artifact;
    }

}
