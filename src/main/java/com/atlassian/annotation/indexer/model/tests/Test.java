package com.atlassian.annotation.indexer.model.tests;


import java.util.HashSet;
import java.util.Set;

/**
 * TODO: Document this class / interface here
 *
 * @since v6.3
 */


public class Test {

    long id;

    String name;

    String jar;

    String testType;

    String testPackage;

    String testClass;

    Set<String> categories;

    String sourceUrl;

    protected Test() {}

    public Test(String jar, String name, String testPackage, String testClass){
        categories = new HashSet<String>();
        this.name = name;
        this.jar = jar;
        this.testPackage = testPackage;
        this.testClass = testClass;
    }

    public Test(long id, String jar, String name, String testType){
        categories = new HashSet<String>();
        this.id = id;
        this.name = name;
        this.jar = jar;
        this.testType = testType;
    }

    public void addCategory(String category){
        categories.add(category);
    }


    public String getTestPackage() {
        return testPackage;
    }

    public String getSourceUrl() {
        return sourceUrl;
    }

    public void setSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }

    public void setTestPackage(String testPackage) {
        this.testPackage = testPackage;
    }

    public String getTestClass() {
        return testClass;
    }

    public void setTestClass(String testClass) {
        this.testClass = testClass;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getJar() {
        return jar;
    }

    public void setJar(String jar) {
        this.jar = jar;
    }

    public Set<String> getCategories() {
        return categories;
    }

    public void setCategories(Set<String> categories) {
        this.categories = categories;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTestType() {
        return testType;
    }

    public void setTestType(String testType) {
        this.testType = testType;
    }
}
