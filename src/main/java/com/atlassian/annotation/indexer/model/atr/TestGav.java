package com.atlassian.annotation.indexer.model.atr;


public class TestGav {

    String[] actions;
    String artifactId;
    String[] categories;
    boolean categoryOverridden;
    String data_version;
    String groupId;
    String java;
    String maven;
    String name;
    String[] products;
    String version;
    boolean ignoreTestArtifact;

    public boolean isIgnoreTestArtifact() {
        return ignoreTestArtifact;
    }

    public void setIgnoreTestArtifact(boolean ignoreTestArtifact) {
        this.ignoreTestArtifact = ignoreTestArtifact;
    }

    public String[] getActions() {
        return actions;
    }

    public void setActions(String[] actions) {
        this.actions = actions;
    }

    public String getArtifactId() {
        return artifactId;
    }

    public void setArtifactId(String artifactId) {
        this.artifactId = artifactId;
    }

    public String[] getCategories() {
        return categories;
    }

    public void setCategories(String[] categories) {
        this.categories = categories;
    }

    public boolean isCategoryOverridden() {
        return categoryOverridden;
    }

    public void setCategoryOverridden(boolean categoryOverridden) {
        this.categoryOverridden = categoryOverridden;
    }

    public String getData_version() {
        return data_version;
    }

    public void setData_version(String data_version) {
        this.data_version = data_version;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getJava() {
        return java;
    }

    public void setJava(String java) {
        this.java = java;
    }

    public String getMaven() {
        return maven;
    }

    public void setMaven(String maven) {
        this.maven = maven;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String[] getProducts() {
        return products;
    }

    public void setProducts(String[] products) {
        this.products = products;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
