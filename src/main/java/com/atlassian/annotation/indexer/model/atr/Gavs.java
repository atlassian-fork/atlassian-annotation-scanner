package com.atlassian.annotation.indexer.model.atr;

public class Gavs {

    private String environment;
    private TestGav[] test_gavs;

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public TestGav[] getTest_gavs() {
        return test_gavs;
    }

    public void setTest_gavs(TestGav[] test_gavs) {
        this.test_gavs = test_gavs;
    }
}
